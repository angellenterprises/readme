# Bulk Migration

For property managers with more than 10 properties we offer an additional way to integrate your properties with Nightpricer.

Once you account is finished migrating you will receive an email containing a list of properties. You will need to update the each property and upload the list into Nighpricer.

## From Email

1. Open Email & Download the CSV attachment
3. Open the file & Review/Edit the following fields
    * **basePrice**: The base price the algorithm will use to determine the nightly rate
    * **minLocked**: The minimum amount a calendar night will be priced for all dates before Fire Sale
      * **minPrice**: When selected, the algorithm  cannot lower the nightly rate below the set minimum, even during Fire Sale
    * **maxLocked**: The maximum amount a calendar night will be priced for any date except Event Dates
      * **maxPrice**: When selected, the algorithm cannot increase the nightly rate above the set maximum, even during for Event Dates
    * **eventMin**: The minimum amount a event calendar night will be priced for all dates before Fire Sale
    * **eventDaysAway**: The days away the event price will be "dropped"
    * **algoDays**: This sets the frequency of base/min/max price updates. It is the number of days to wait before updating (7 days is recommended)
    * **priceSync**: Allows you to choose whether or not the Nightpricer algorithm should be active and sync specific  properties
4. Navigate back to the Nightpricer <a href="https://app.nightpricer.com/app/properties" target="_blank">Dashboard</a>.
5. Click "Upload" and select the file you edited.

## From Template Download

If you do not receive the initial email to bulk upload property details, you may also download the csv of properties from the properties list page. Once you download the list, you will need to update each property and upload the list into Nighpricer.

1. Navigate to the Nightpricer <a href="https://app.nightpricer.com/app/properties" target="_blank">Dashboard</a>.
2. Click "Download"
3. Open the file & Review/Edit the following fields
    * **basePrice**: The base price the algorithm will use to determine the nightly rate
    * **minLocked**: The minimum amount a calendar night will be priced for all dates before Fire Sale
      * **minPrice**: When selected, the algorithm  cannot lower the nightly rate below the set minimum, even during Fire Sale
    * **maxLocked**: The maximum amount a calendar night will be priced for any date except Event Dates
      * **maxPrice**: When selected, the algorithm cannot increase the nightly rate above the set maximum, even during for Event Dates
    * **eventMin**: The minimum amount a event calendar night will be priced for all dates before Fire Sale
    * **eventDaysAway**: The days away the event price will be "dropped"
    * **algoDays**: This sets the frequency of base/min/max price updates. It is the number of days to wait before updating (7 days is recommended)
    * **priceSync**: Allows you to choose whether or not the Nightpricer algorithm should be active and sync specific  properties
4. Navigate back to the Nightpricer <a href="https://app.nightpricer.com/app/properties" target="_blank">Dashboard</a>.
5. Click "Upload" and select the file you edited.

When you are finished uploading the properties, you will still need to add them to their respective <a href="https://app.nightpricer.com/app/calendars"  target="_blank">calendar</a>.
