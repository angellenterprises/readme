# Getting Started

Currently Nightpricer integrates with property management channels <a href="https://guesty.com/" target="_blank">Guesty</a>, <a href="https://hostaway.com/" target="_blank">Hostaway</a> and <a href="https://hostfully.com/" target="_blank">Hostfully</a>.

## Provider Integration

### Guesty Users

[![Guesty passing!](https://img.shields.io/badge/Guesty-passing-green)](https://app.nightpricer.com)

> If you dont have a Guesty account sign up <a href="https://guesty.com" target="_blank">here</a>.

To integrate Nightpricer with Guesty you will need a client ID, and a client secret. These can be generated on the api page of the guesty dashboard <a href="https://app.guesty.com/integrations/api" target="_blank">here</a>.

<img src="/public/static/help/integrations/guesty/activate.png" alt="Activate Guesty" width="80%"/>

- Ex. Client ID: String - Ex - 3h89fh2479gh2498gh948h948g92843ghf
- Ex. Client Secret: String - Ex - 3498rht23984gt9283gt9823gt98239238923g9823823g

Once you copy your Client ID and Secret paste them info the fields on the "Get Started" Dialog and press "Migrate" to migrate your properties into Nightpricer.

> Properties integrated with Guesty after the initial migration with Nightpricer will automatically appear under "Inactive" tab once the property is active in the Guesty dashboard.


### Hostaway Users

[![Hostaway passing!](https://img.shields.io/badge/Hostaway-passing-green)](https://app.nightpricer.com)

> If you dont have a Hostaway account sign up <a href="https://hostaway.com" target="_blank">here</a>.

To integrate Nightpricer with Hostfully you will need to complete 2 steps.

First you will need to create a new API key for Nightpricer. This can be done in the Hostaway dashboard by clicking <a href="https://dashboard.hostaway.com/settings/hostaway-api" target="_blank">here</a>.

<img src="/public/static/help/integrations/hostaway/activate.png" alt="Activate Hostaway" width="80%"/>

- Ex. Client ID: String - Ex: 12345
- Ex. Client Secret: String - Ex: 3498rht23984gt9283gt9823gt98239238923g9823823g

Once you copy your Client ID and Secret paste them info the fields on the "Get Started" Dialog and press "Migrate" to migrate your properties into Nightpricer.

<img src="/public/static/help/integrations/hostaway/gs.gif" alt="Migrate Hostaway" width="50%"/> 

> Properties integrated with Hostaway after the initial migration will automatically appear under "Inactive" once the property is active in the Hostaway dashboard.

### Hostfully Users

[![Hostfully passing!](https://img.shields.io/badge/Hostfully-passing-green)](https://app.nightpricer.com)

> If you dont have a Hostfully account sign up <a href="https://hostfully.com" target="_blank">here</a>.

To integrate Nightpricer with Hostfully you will need to complete 2 steps.

First you will need to activate the Nightpricer integration in the hostfully dashboard. This can be done <a href="https://platform.hostfully.com/integrations.jsp" target="_blank">here</a>.

<img src="/public/static/help/integrations/hostfully/activate.png" alt="Activate Hostfully" width="80%"/>

Then you will need to copy your agency UID. This can be found at the bottom of your "Agency Settings" page in the Hostfully dashboard <a href="https://platform.hostfully.com/agency.jsp" target="_blank">this page</a>.

<img src="/public/static/help/integrations/hostfully/keys.png" alt="Migrate Hostfully" width="80%"/>

- Ex. Agency UID: f3e23r23r-3232-4344-1313-r34r34r34r

Once you copy your Agency UID paste it info the field on the "Get Started" Dialog and press "Migrate" to migrate your properties into Nightpricer.

<img src="/public/static/help/integrations/hostfully/gs.gif" alt="Activate Hostfully" width="50%"/> 

> Properties integrated with Hostfully after the initial migration with Nightpricer will automatically appear under "Inactive" tab once the property is active in the Hostfully dashboard.

## Update Default Pricing

### Updating Property

1. Navigate to the Nightpricer dashboard <a href="https://app.nightpricer.com/app/properties"  target="_blank">properties</a> page.
2. Click on the "Inactive" tab.
3. Click the "Update" button on the property you would like to review and review the following fields;  

<img src="/public/static/help/integrations/gs_update.gif" alt="Update Properties" width="50%"/>  

DEFINITIONS:  

  * **basePrice**: The base price the algorithm will use to determine the nightly rate
  * **minLocked**: The minimum amount a calendar night will be priced for all dates before Fire Sale
    * **minPrice**: When selected, the algorithm  cannot lower the nightly rate below the set minimum, even during Fire Sale
  * **maxLocked**: The maximum amount a calendar night will be priced for any date except Event Dates
    * **maxPrice**: When selected, the algorithm cannot increase the nightly rate above the set maximum, even during for Event Dates
  * **eventMin**: The minimum amount a event calendar night will be priced for all dates before Fire Sale
  * **eventDaysAway**: The days away the event price will be "dropped"
  * **priceSync**: Allows you to choose whether or not the Nightpricer algorithm should be active and sync specific  properties

### Finish Up

Once you have updated all the defaults for your properties you will need to add events to your default calendar and then select the properties you want to attach to that calendar calendar.

<img src="/public/static/help/integrations/gs_finish.gif" alt="Finish Properties" width="50%"/> 

## Calendar + Events

To maximize nightly rates for event dates, create a calendar, and enter any upcoming events, holidays, etc. that may be more valuable in your market then add the properties in that market.

The algorithm uses your custom event days to price to the upcoming events nights. The price for the event day is set on each individual property. You can create the events by visiting your <a href="https://app.nightpricer.com/app/calendars" target="_blank">calendar</a>.

Please follow the next steps to finalize your migration.

### Add Events

Select the dates you would like to make into an event and add a name for the event.

<img src="/public/static/help/integrations/gs_events.gif" alt="Calendar Events" width="50%"/>

### Add Properties to Calendar

Select the properties you would like to add to your default calendar.

<img src="/public/static/help/integrations/gs_activate.gif" alt="Calendar Activate" width="50%"/>  

> If you would like to create multiple calendars, please reach out to customer service for additional features.