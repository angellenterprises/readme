# Billing & Subscription

*Last revised on February 16, 2021, effective as of February 16, 2021*

Nightpricer is an application that continuously updates nightly pricing and finds the specific market value for short term rentals.

- Each subscription includes all available tools.

- Fees collected by Nightpricer at the end of each billing period are calculated by applying the predetermined percentage of each connected property/listing’s total monthly gross revenue.

- The predetermined percentage that is charged monthly is based on the total number of properties integrated with Nightpricer.

- Reservations that are already in place before syncing with Nightpricer are not included in the billable gross revenue.

- Reservations booked and completed within an active cycle are included in the billable gross revenue.

- Billing cycles begin at 12:00am on the 1st day of each month and end at 11:59pm on the last day of each month.

- Statements include all billable revenue from the preceding month and are provided within 5 days of the corresponding billing period.

### Billable Reservations

At the end of each month we calculate "billable reservations" by reviewing the snapshots taken when you enabled and disabled the price sync feature for each property.  We call this the **active cycle**.

The **active cycle** starts when the price sync feature is turned on and ends **72 hours** after the price sync feature is disabled. Each month a property can have multiple **active cycles**.

- If you enable price syncing for the whole month, each reservation **booked** and **completed** in the billing cycle would be subject to our fee.

- If you enable price syncing and later disable it you are only charged for reservations **booked** and **completed** during the active cycles.

- If you throttle price sync on and off, we record this information and again, you will only be charged for reservations **booked** and **completed** inside the active cycles.

***

*When the price sync feature is disabled, a snapshot is taken and any booking made at the exact price of the snapshot, during the billing cycle, will also be considered a billable reservation.


### Free Trial

Your free trial starts the day your account is registered. The number of free trial days is currently 30.

### Payment Information

In order to use the payment functionality of Nightpricer, you must open a "Dwolla Platform" account provided by Dwolla,Inc. and you must accept the <a href="https://www.dwolla.com/legal/tos/" target="_blank>"​D​wolla Terms of Service​<a/> and​ <a href="https://www.dwolla.com/legal/tos/" target="_blank>P​rivacy Policy​<a/>.​ Any funds held in the Dwolla account are held by Dwolla's financial institution partners as set out in the​ ​Dwolla Terms of Service.​ You authorize Nightpricer to collect and share with Dwolla your personal information including full name, email address and financial information, and you are responsible for the accuracy and completeness of that data. You understand that you will access and manage your Dwolla account through Nightpricer, and Dwolla account notifications will be sent by Nightpricer, not Dwolla. Nightpricer will provide customer support for your Dwolla account activity, and can be reached at nightpricer.com, support@nightpricer.com and/or 1 (314) 954-8393.

### Other Information

There are no contracts for Nightpricer. The billing is month to month and you can cancel any time. The final charges for the current month will be charged to you at the end of the billing month. See our guide <a href="https://app.nightpricer.com/help">here</a> for information on pausing or canceling your account.

### Dispute Resolution

We understand that allowing you to enable and disable the pricing features creates active cycles which can cause billing issues.

If you have any problems with your statement, you have questions about your statement or you would like to give us input, please reach out to us at <a mailto="support@nightpricer.com">support@nightpricer.com</a>.

Sincerely,

Josh Sedivy - Co-Founder
