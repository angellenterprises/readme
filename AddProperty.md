# Add New Property

Once you have enabled the property in the property manager it should be visible in the property list under the "inactive" tab.

> If you property isnt showing up, verify the property is enabled in the PM and refresh the page.

## Step 1. Set Property Defaults

1. Navigate to the Nightpricer <a href="https://app.nightpricer.com/app/properties"  target="_blank">Dashboard</a>.
2. Click the "Inactive" tab
3. Click the "Set Defaults" button and review/edit the following fields
    * **basePrice**: The base price the algorithm will use to determine the nightly rate
    * **minLocked**: The minimum amount a calendar night will be priced for all dates before Fire Sale
      * **minPrice**: When selected, the algorithm  cannot lower the nightly rate below the set minimum, even during Fire Sale
    * **maxLocked**: The maximum amount a calendar night will be priced for any date except Event Dates
      * **maxPrice**: When selected, the algorithm cannot increase the nightly rate above the set maximum, even during for Event Dates
    * **eventMin**: The minimum amount a event calendar night will be priced for all dates before Fire Sale
    * **eventDaysAway**: The days away the event price will be "dropped"
    * **algoDays**: This sets the frequency of base/min/max price updates. It is the number of days to wait before updating (7 days is recommended)
4. Click "Update"

## Step 2. Activate Property

In order for the property to become active you will need to add the property to a calendar.

> If you do not want the property to sync with events, you can create a new Calendar and add the property there.

1. Click the "Finish Migration" button on the inactive property list or navigate to your calendars
2. Click the "Add Properties" button in the "Active Properties" list
3. Search for the property or scroll to the property.
4. Click the checkmark next to the property you would like to add.
5. Press "Update"

> Any added properties will automatically sync on update.
