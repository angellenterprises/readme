# Terms of Service

*Last revised on October 31, 2020, effective as of October 31, 2020*

By accessing, browsing or using this Site, you acknowledge that you have read, understood and agreed to be bound by these Terms of Use (these “Terms”). If you do not agree to these Terms, you should not use or access this Site. Nightpricer reserves the right to revise these Terms at any time by updating this posting. You are encouraged to review these Terms each time you use the Site because your use of the Site after the posting of changes will constitute your acceptance of the changes. By agreeing to these Terms it also constitutes your agreement to the Site Privacy Policy, which are incorporated herein.

BY ACCESSING OR USING ANY PART OF THE SITE, YOU ACCEPT, WITHOUT LIMITATION OR QUALIFICATION, THESE TERMS. IF YOU DO NOT AGREE WITH ALL OF THE TERMS SET FORTH BELOW, YOU MAY NOT USE ANY PORTION OF THE SITE. PRICE INFORMATION FOUND ON THIS SITE IS SUBJECT TO CHANGE WITHOUT NOTICE. NIGHTPRICER RESERVES THE RIGHT TO CHANGE THESE TERMS & CONDITIONS OF USE AT ANY TIME WITHOUT NOTICE.

We grant you a personal, limited, non-transferable non-exclusive, license to access and use the Site. We reserve the right, in our sole discretion and without notice to you, to revise the products and services available on the Site and to change, suspend or discontinue any aspect of the Site and we will not be liable to you or to any third party for doing so. We may also impose rules for and limits on use of the Site or restrict your access to part, or all, of the Site without notice or penalty. Your continued use of the Site will constitute your acceptance of any such changes.

You may use the Site only for your own noncommercial personal use and in compliance with these Terms. You are responsible for your own communications, including the transmission, uploading or posting of information and are responsible for the consequences of such communications to the Site. Any other use of the Site requires the prior written consent of Nightpricer. You may not otherwise copy, modify, or distribute the contents of this Site without the express written permission of Nightpricer. You may not modify, publish, transmit, participate in the transfer or sell, create derivative works from, or in any way exploit, any of the content, in whole or in part, found on the Site.

We require all visitors to agree not to use the Site, and specifically prohibit any use of the Site, for any of the following purposes:

- Posting, communicating or transmitting any material that infringes on any intellectual property, publicity or privacy right of another person or entity

- Posting any information which is untrue, inaccurate or not your own

- Engaging in conduct that would constitute a criminal offense or give rise to civil liability or otherwise violate any law or regulation

- Attempting to interfere in any way with the Site’s or Nightpricer’s network security, or attempting to use the Site’s service to gain unauthorized access to any other computer system

You may not use spiders, robots, data mining techniques or other automated devices or programs to catalog, download or otherwise reproduce, store or distribute content available on the Site. Further, you may not use any such automated means to manipulate the Site, such as automating what are otherwise manual or one-off procedures. You may not take any action to interfere with, or disrupt, the Site or any other user's use of the Site, including, without limitation, via means of overloading, “flooding”, “mailbombing” or “crashing” the Site, circumventing security or user authentication measures or attempting to exceed the limited authorization and access granted to you under these Terms. You may not frame portions of the Site within another web site. You may not resell use of, or access to, the Site to any third party without our prior written consent.

## Additional Terms and Conditions

You agree that additional terms and conditions may apply to properties, calendars or your use of certain portions of the Site, including with respect to billing, dispute resolution policies, and review guidelines (“Additional Terms”), which Additional Terms are made part of these Terms by reference. If there is a conflict between these Terms and the Additional Terms, the Additional Terms shall control.

## Service Eligibility

By using our Site and Content, you agree that: (1) you must be the "Minimum Age"(described below) or older; (2) you will only have one Nightpricer account, which must be in your legal business name; and (3) you are not already restricted by Nightpricer from using the Site. Creating an account with false information is a violation of our terms, including accounts registered on behalf of others or persons under the age of 18.

“Minimum Age” means 18 years old. However, if law requires that you must be older in order for Nightpricer to lawfully provide the Site to you without parental consent (including using of your personal data) then the Minimum Age is such older age.

## Your Account

By using our Site and Content, you choose to provide us with private account information. If you choose to automate the pricing service, you give us the permission to update pricing information on your behalf. We will not change any other information on your related account.

Personal information that you supply to us will not be disclosed by us to any third party save in accordance with our Privacy Policy. You agree that we may use the personal information supplied by you in accordance with our Privacy Policy

## Your Listing

We are not responsible for the success of your listing, and profits. By using our Site and Content, you understand that optimization is all about probability. We will operate the Site, Content and pricing information with reasonable skill. You will continue to have control over your pricing information and can update it as you deem appropriate.

By using our Site and Content, you understand that we are not responsible for guests who use your listing. You (or who you choose to authorize), and not Nightpricer, will be responsible for any bookings or maintenance of records.

## Proprietary Rights

You acknowledge and agree that the content, materials, text, images, videos, graphics, trademarks, logos, button icons, music, software and other elements available on the Site are the property of Nightpricer or our licensors and are protected by copyright, trademark and/or other proprietary rights and laws. You agree not to sell, license, rent, distribute, copy, reproduce, transmit, publicly display, publicly perform, publish, adapt, edit, modify or create derivative works from any content or materials on the Site. Nightpricer and the Nightpricer logo are registered trademarks. All other trademarks are the property of their respective owners. All of our Site's content is Copyright 2021 Nightpricer, LLC All rights reserved. Except as expressly set forth in these Terms, no license is granted to you and no rights are conveyed by virtue of accessing or using the Site. All rights not granted under these Terms are reserved by Nightpricer.

## Disclaimers

You assume all responsibility and risk with respect to your use of the Site. THE SITE, AND ALL CONTENT, MERCHANDISE, AND OTHER INFORMATION ON OR ACCESSIBLE FROM OR THROUGH THIS SITE OR A “LINKED” SITE ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, SECURITY OR ACCURACY. SPECIFICALLY, BUT WITHOUT LIMITATION, Nightpricer DOES NOT WARRANT THAT: (1) THE INFORMATION ON THIS SITE IS CORRECT, ACCURATE OR RELIABLE; (2) THE FUNCTIONS CONTAINED ON THIS SITE WILL BE UNINTERRUPTED OR ERROR-FREE; OR (3) DEFECTS WILL BE CORRECTED, OR THAT THIS SITE OR THE SERVER THAT MAKES IT AVAILABLE IS FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. Nightpricer makes no warranties of any kind regarding any non-Nightpricer sites to which you may be directed or hyperlinked from this Site. Hyperlinks are included solely for your convenience, and Nightpricer makes no representations or warranties with regard to the accuracy, availability, suitability or safety of information provided in such non-Nightpricer sites. Nightpricer does not endorse, warrant or guarantee any products or services offered or provided by or on behalf of third parties on the Site.

## Taxes

Your total price will include the price of the product plus any applicable sales tax; such state and local sales tax is based on the shipping address and the sales tax rate in effect at the time you purchase the product. Currently a flat tax rate is in effect.

## Indemnification

You agree to indemnify, hold harmless, and defend Nightpricer  its parent, subsidiaries, divisions, and affiliates, and their respective officers, directors, employees, agents and affiliates from any and all claims, liabilities, damages, costs and expenses of defense, including attorneys’ fees, in any way arising from or related to your use of the Site, your violation of these Terms or the Privacy Policy, content posted to the Site by you, or your violation of any law or the rights of a third party.

IN NO EVENT SHALL NIGHTPRICER, ITS PARENT COMPANY, SUBSIDIARIES, AFFILIATES OR ANY OF THEIR RESPECTIVE OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, SUCCESSORS, SUBSIDIARIES, SUPPLIERS, AFFILIATES, OR THIRD PARTIES PROVIDING INFORMATION ON THIS SITE BE LIABLE TO ANY USER OF THE SITE OR ANY OTHER PERSON OR ENTITY FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE, CONSEQUENTIAL OR EXEMPLARY DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, LOSS OF DATA, OR LOSS OF USE) ARISING OUT OF THE USE OR INABILITY TO USE THE SITE, WHETHER BASED UPON WARRANTY, CONTRACT, TORT, OR OTHERWISE, EVEN IF Nightpricer HAS BEEN ADVISED OF OR SHOULD HAVE KNOWN OF THE POSSIBILITY OF SUCH DAMAGES OR LOSSES. IN NO EVENT SHALL THE TOTAL LIABILITY OF Nightpricer  ITS AFFILIATES OR ANY OF THEIR RESPECTIVE OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, SUCCESSORS, SUBSIDIARIES, SUPPLIERS, AFFILIATES OR THIRD PARTIES PROVIDING INFORMATION ON THIS SITE TO YOU FOR ALL DAMAGES, LOSSES, AND CAUSES OF ACTION RESULTING FROM YOUR USE OF THIS SITE, WHETHER IN CONTRACT, TORT (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE) OR OTHERWISE, EXCEED THE AMOUNT YOU PAID TO NIGHTPRICER IN CONNECTION WITH THE EVENT GIVING RISE TO SUCH LIABILITY.

You hereby acknowledge that the preceding paragraph shall apply to all content, merchandise and services available through the Site. Because some states do not allow limitations on implied warranties or the exclusion or limitation of certain damages, all of the above disclaimers or exclusions may not apply to all users.

## International Use

We control and operate the Site from the United States. We make no representation that materials on the Site are appropriate or available for use outside the United States. If you choose to access this Site from outside the United States, you do so at your own initiative and are responsible for compliance with local laws, if and to the extent local laws are applicable.

## Risk of Loss

Any merchandise purchased from our Site will be shipped by a third party carrier. As a result, title and risk of loss for such merchandise will pass to you upon our delivery to the carrier.

## Copyright Infringement; Notice and Take Down Procedures

Nightpricer specifically prohibits the posting of any content that violates or infringes the copyright rights and/or other intellectual property rights (including rights of privacy and publicity) of any person or entity. If you believe that any material contained on this Site infringes your copyright or other intellectual property rights, you should notify us of your copyright infringement claim in accordance with the following procedure. Nightpricer will process notices of alleged infringement which it receives and will take appropriate action as required by the Digital Millennium Copyright Act (“DMCA”). The DMCA requires that notifications of claimed copyright infringement should be sent to the following address:

Nightpricer, LLC<br />
2915 Magnolia Avenue<br />
St. Louis, MO 63118<br />
1 (314) 954-8393

## Severability

If any part of these Terms shall be held or declared to be invalid or unenforceable for any reason by any court of competent jurisdiction, such provision shall be ineffective but shall not affect any other part of these Terms, and in such event, such provision shall be changed and interpreted so as to best accomplish the objectives of such unenforceable or invalid provision within the limits of applicable law or applicable court decisions.

## Waiver; Remedies


The failure of Nightpricer to partially or fully exercise any rights or the waiver of Nightpricer of any breach of these Terms and Conditions by you shall not prevent a subsequent exercise of such right by Nightpricer or be deemed a waiver by Nightpricer of any subsequent breach by you of the same or any other term of these Terms. The rights and remedies of Nightpricer under these Terms and any other applicable agreement between you and Nightpricer shall be cumulative, and the exercise of any such right or remedy shall not limit Nightpricer’s right to exercise any other right or remedy.


## Governing Law


The laws of the State of Missouri shall govern these Terms.

YOU HEREBY EXPRESSLY CONSENT TO EXCLUSIVE JURISDICTION AND VENUE IN THE COURTS LOCATED IN ST. LOUIS COUNTY, MISSOURI FOR ALL MATTERS ARISING IN CONNECTION WITH THESE TERMS AND CONDITIONS OR YOUR ACCESS OR USE OF THE SITE.


Questions: Should you have any questions regarding these Terms you may contact us at <a href="mailto:support@nightpricer.com">support@nightpricer.com</a>
