# Privacy Policy

*Last revised on October 31, 2020, effective as of October 31, 2020*

Nightpricer (“we” or “us”) takes the privacy of your information very seriously. This policy explains how and for what purposes we use the information collected about you via this site (referred to below as the “Site”). Please read this privacy policy carefully. By using the Site and any services we offer via the Site, you are agreeing to be bound by this policy in respect of the information collected about you via this Site. If you have any queries about the policy, please get in touch with us using our contact details and we will do our best to answer your questions.

### Information collected

We will collect the following personal information from you:

- Certain information required to register with the Site including full name, your email address, and password;
- Billing information such as your credit card number and expiry date;
- Information about your listings, including availability, occupancy, prices, reservations/bookings, and some other basic characteristics about your property such as location, amenities, reviews etc.

### Use of this information

We (or Nightpricer) will use this information to obtain price recommendations and manage prices and minimum stay restrictions for your listings. We may use anonymized reservation data to understand trends and improve our predictions.

### Sharing this information

We do not sell or share your personal or listing information to third-parties. If we are involved in a sale of some or all of our business and assets to a third party, or are a part of any business restructuring or reorganization, we may transfer your personal information to the said third party. We will take steps with the aim of ensuring that your privacy rights continue to be protected, and notify you about those.

### Sharing log information

Log files/IP addresses. When you visit the Site our web server automatically records your IP address. This IP address is not linked to any personal information. It helps us serve you better by understanding your general location and type of browser.
Cookies. When you visit the Site we may store some information (commonly known as a “cookie”) on your computer. Cookies are pieces of information that a website transfers to your hard drive to store and sometimes track information about you. Cookies are specific to the server that created them and cannot be accessed by other servers, which means that they cannot be used to track your movements around the web. We use cookies for enabling our web server to track your session between pages of the site and provide a continuity of experience. Passwords and credit card numbers are not stored in cookies.
You can block or erase cookies from your computer if you want to (your browser’s help screen or manual should tell you how to do this), but certain parts of the Site are reliant on the use of cookies to operate correctly and may not work correctly if you set your browser not to accept cookies.
